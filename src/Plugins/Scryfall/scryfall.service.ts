import http from 'http';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ScryfallService {
    base_url = 'https://api.scryfall.com';

    /**
     * Fetch a specific card from the Scryfall API
     */
    fetchCard(oracleId: string)
    {
        return new Promise((resolve, reject) => {
            http.get(this.base_url + '/cards/' + oracleId, (response) => {
                let data = '';
                response.on('data', chunk => data += chunk);
                response.on('end', () => resolve(JSON.parse(data)));
            });
        });
    }
}