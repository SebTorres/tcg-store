import { PluginConfigurationFn, VendurePlugin } from '@vendure/core';
import { Card } from './card';
import { CardsController } from './cards-controller';
import { CardsService } from './cards-service';

@VendurePlugin({
  entities: [Card],
  controllers: [CardsController],
  providers: [CardsService],
})

export class CardsPlugin {}
