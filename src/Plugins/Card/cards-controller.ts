import { endpoint } from '@airtasker/spot';
import { Controller, Get, Post, Request, Body } from '@nestjs/common';
import { Ctx, RequestContext } from '@vendure/core';
import { Card } from './card';
import { CardsService } from './cards-service';
import CardRequest from './dto/card-request.dto';

@Controller('cards')
export class CardsController {
    constructor(private cardservice: CardsService) {}

    @Post('add-stock')
    addStock(
        @Body() card: CardRequest,
    ): Promise<Card>
    {
        return this.cardservice.addStock(card);
    }

}