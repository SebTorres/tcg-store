import { IsNotEmpty, IsNumber, IsUUID, Min, IsOptional } from 'class-validator';

export default class CardRequest {
    @IsNotEmpty()
    _id: string;

    @IsOptional()
    @IsNumber()
    near_mint?: number;

    @IsOptional()
    @IsNumber()
    lightly_played?: number;

    @IsOptional()
    @IsNumber()
    played?: number;
}