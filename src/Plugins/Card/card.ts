import { VendureEntity } from "@vendure/core";
import { DeepPartial } from '@vendure/common/lib/shared-types';
import { Column, Entity } from 'typeorm';

@Entity()
export class Card extends VendureEntity
{
  constructor(input?: DeepPartial<Card>) {
    super(input);

    this.near_mint = input?.near_mint ? input.near_mint : 0;
    this.lightly_played = input?.lightly_played ? input.lightly_played : 0;
    this.played = input?.played ? input.played : 0;
  }

  @Column()
  oracle_id: string;

  @Column()
  near_mint: number;

  @Column()
  lightly_played: number;

  @Column()
  played: number;
}