import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Card } from './card';
import CardRequest from './dto/card-request.dto';


@Injectable()
export class CardsService {
    constructor(
        @InjectRepository(Card)
        private readonly card: Repository<Card>,
    ) {}

    /**
     * This endpont adds stock for cards in the database.
     * @param card: The Card to update.
     * @returns Card
     */
    async addStock(card: CardRequest): Promise<Card>
    {
        // Find out if the card already exists in the database
        let localCard = await this.card.findOne({
            where: {
                _id: card._id,
            }
        });

        if (!localCard) {
            return (await this.card.insert(card)).generatedMaps[0] as Card;
        } else {
            return this.card.save(
                {
                    id: localCard.id,
                    near_mint: localCard.near_mint + ( card.near_mint || 0 ),
                    lightly_played: localCard.lightly_played + ( card.lightly_played || 0),
                    played: localCard.played + ( card.played || 0 ),
                }
            );
        }
    }
}