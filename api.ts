import { api, body, endpoint, request, response, String } from "@airtasker/spot";
import { Card } from "src/Plugins/Card/card";

@api({ name: "my-api" })
class Api {}

@endpoint({
  method: "POST",
  path: "/cards/add-stock",
  tags: ["Cards"]
})
class AddStock {
  @request
  request(
    @body body: CardRequest
  ) {}

  @response({ status: 200 })
  successfulResponse(
    @body body: Card
  ) {}
}

interface CardRequest {
  oracle_id: String;
  near_mint: Number;
  lightly_played: Number;
  played: Number;
}

interface Card {
  id: string;
  oracle_id: String;
  near_mint: Number;
  lightly_played: Number;
  played: Number;
}
